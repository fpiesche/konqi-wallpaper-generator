import glob
import hashlib
import numpy
import os
from PIL import Image, ImageFilter, ImageEnhance
from random import choice, randrange, random
import struct

target_size = (1920, 1080)


def colorMix(start, end, paletteSize):    # creates a list of colors that gradually fade from top_left to top_right, inclusive. paletteSize is the amount of values that will be generated.
               # The smallest useful paletteSize is 3, as it will return [top_left, top_leftMixedWithtop_right, top_right]
        palette = [start]
        colorDifference = [ start[0] - end[0], start[1] - end[1], start[2] - end[2] ]

        Rstep = (start[0] - end[0]) / (paletteSize -1)
        Gstep = (start[1] - end[1]) / (paletteSize -1)
        Bstep = (start[2] - end[2]) / (paletteSize -1)

        for i in range(1, paletteSize):
            palette.append((start[0] - Rstep*i, start[1] - Gstep*i, start[2] - Bstep*i))

        # palette.append(end)

        return palette


def createColorGrid(resolution, top_left, top_right, bottom_right, bottom_left):        # build a new colorGrid using a different process than above. colors are RGB format. For a 1D color fade set pairs of colors
                                      # like (255,0,0) (255,0,0) (0,255,255) (0,255,255). Colors are ordered from top left corner and follow corners clockwise.

        print('Colours: {}, {}, {}, {}'.format(top_left, top_right, bottom_left, bottom_right))

        print('Generating gradient @{}x{}'.format(resolution[0], resolution[1]))
        top_left = hex_to_rgb(top_left)
        top_right = hex_to_rgb(top_right)
        bottom_left = hex_to_rgb(bottom_left)
        bottom_right = hex_to_rgb(bottom_right)

        colorArray = []
        leftColumn = colorMix(top_left,bottom_left,resolution[1])
        rightColumn = colorMix(top_right,bottom_right,resolution[1])

        for i in range(0, resolution[1]):                                                                  # color processing goes from top left to top right, then down a row and repeat
            colorArray.append(colorMix(leftColumn[i],rightColumn[i],resolution[0]))
        print('Converting gradient to numpy array...')
        return numpy.array(colorArray)


def hex_to_rgb(hex_string):
    return struct.unpack('BBB',bytes.fromhex(hex_string[1:]))


def get_random_position(background, artwork):
    print('Finding placement for art @{}x{}...'.format(artwork.width, artwork.height))
    positions = [
        (0, 0), # top_left
        # ((background.width / 2) - (artwork.width / 2), 0), # top_center
        (background.width - artwork.width, 0), # top_right
        (0, (background.height / 2) - (artwork.height / 2)), # middle_left
        ((background.width / 2 - artwork.width / 2), (background.height / 2 - artwork.height / 2)), # center
        (0, background.width - artwork.width), # middle_right
        (0, background.height - artwork.height), # bottom_left
        # (background.width / 2 - artwork.width / 2, background.height - artwork.height), # bottom_center
        (background.width - artwork.width, background.height - artwork.height) # bottom_right
    ]

    placement = choice(positions)
    print('Selected base placement {}'.format(placement))

    return (round(placement[0]), round(placement[1]))

def place_artwork(file_name, background):
    artwork = Image.open(file_name)

    bounding_box = artwork.getbbox()
    cropped = artwork.crop(bounding_box)

    if (artwork.width > background.width or artwork.height > background.height):
        maxsize = (round(background.width * 0.8), round(background.height * 0.8))
        minsize = (round(background.width * 0.4), round(background.height * 0.4))

        # TODO: pick which dimension to resize by
        dimension = artwork.height
        min_dim = minsize[1]
        max_dim = maxsize[1]

        # pick a random target size between minimum and maximum
        ratio = randrange(min_dim, max_dim) / dimension
        print('Resizing artwork by {}x...'.format(ratio))

        artwork = artwork.resize((round(artwork.width * ratio), round(artwork.height * ratio)))

    padding = (round(background.width / 50), round(background.height / 50))

    target_size = (artwork.width + padding[0], artwork.height + padding[1])
    artwork_padded = Image.new('RGBA', target_size)
    artwork_padded.paste(artwork, (round(padding[0]/2), round(padding[1]/2)))

    target = Image.new('RGBA', background.size)

    target.paste(artwork, get_random_position(background, artwork))
    return target

def white_noise(width, height, alpha=32):
    print('Generating random noise @{}x{}...'.format(width, height))
    noise = Image.new('RGBA', (width, height))
    random_grid = map(lambda x: (
            int(random() * 256),
            int(random() * 256),
            int(random() * 256),
            int(random() * alpha) if alpha else 255
        ), [0] * width * height)
    noise.putdata(list(random_grid))
    return noise

def edge_clouds(width, height, threshold=32):

    # Generate white noise, upscale and filter
    print('Creating base greyscale noise...')
    noise = white_noise(round(width / 16), round(height / 16), alpha=None)
    noise = noise.convert('L')
    noise = noise.resize((width, height))
    noise = noise.filter(ImageFilter.GaussianBlur(radius=3))

    # Increase the brightness as the scaled noise images tend to be very dark
    print('Finding edges and increasing brightness...')
    noise = noise.filter(ImageFilter.FIND_EDGES)
    enhancer = ImageEnhance.Brightness(noise)
    noise = enhancer.enhance(5)

    # Threshold the image to 1-bit
    print('Thresholding noise to 1-bit colour...')
    noise = noise.point(lambda x : 255 if x > threshold else 0, mode='1')

    # Add a random-noise alpha channel
    print('Adding random noise to alpha channel...')
    noise = noise.convert('RGBA')
    alpha = white_noise(width, height, alpha=None).convert('L')
    enhancer = ImageEnhance.Brightness(alpha)
    alpha = enhancer.enhance(.1)
    noise.putalpha(alpha)

    return noise

def add_noise(image):
    noise_functions = [white_noise, edge_clouds]
    image.alpha_composite(choice(noise_functions)(image.width, image.height))


colours = ['#cb8687', '#6312e9', '#a690fc', '#b56cf2', '#511d86', '#a2d733', '#79e0f5', '#69a4e1',
           '#04737a', '#c6e2e1', '#123c69', '#cbc138', '#7bc9c0', '#dd04dd']
artwork_files = glob.glob(os.path.join(os.path.dirname(__file__), 'konqi_artwork', '*.png'))

print('Generating gradient...')
color_grid = createColorGrid(target_size, choice(colours), choice(colours), choice(colours), choice(colours))

print('Creating images...')
background = Image.fromarray(numpy.uint8(color_grid)).convert('RGBA')
artwork_file = choice(artwork_files)
artwork = place_artwork(artwork_file, background)

print('Adding noise to gradient...')
add_noise(background)

print('Compositing {}...'.format(artwork_file))
background.alpha_composite(artwork)

file_name = '{}.png'.format(hashlib.sha256(background.tobytes()).hexdigest())
print('Writing {}'.format(file_name))
background.save(file_name)
